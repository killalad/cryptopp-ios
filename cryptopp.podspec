#
# Be sure to run `pod lib lint cryptopp.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'cryptopp'
  s.version          = '0.0.3'
  s.summary          = 'c++ cryptopp framework for ios'

  s.description      = <<-DESC
  This pod serves as just includer of c++ cryptopp library, but it doesn't contain any swift or obj-c code.
                       DESC

  s.homepage         = 'https://gitlab.com/killalad/cryptopp-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'killalad' => 'martin.georgiu@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/killalad/cryptopp-ios', :tag => s.version.to_s }

  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.ios.deployment_target = '9.0'
  s.swift_version = '4.0'

  s.preserve_paths = 'cryptopp.framework'
  s.vendored_frameworks = 'cryptopp.framework'
  s.library = 'c++'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework cryptopp' }

  s.source_files = 'cryptopp.framework/Headers/**/*{.h,.hpp}'
  s.public_header_files = 'cryptopp.framework/Headers/**/*{.h,.hpp}'
  s.header_dir = 'cryptopp'
  s.header_mappings_dir = 'cryptopp.framework/Headers/'

end
