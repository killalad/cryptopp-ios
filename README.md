# cryptopp
[![Version](https://img.shields.io/cocoapods/v/cryptopp.svg?style=flat)](https://cocoapods.org/pods/cryptopp)
[![License](https://img.shields.io/cocoapods/l/cryptopp.svg?style=flat)](https://cocoapods.org/pods/cryptopp)
[![Platform](https://img.shields.io/cocoapods/p/cryptopp.svg?style=flat)](https://cocoapods.org/pods/cryptopp)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

cryptopp is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'cryptopp'
```

## Author

killalad, martin.georgiu@gmail.com

## License

cryptopp is available under the MIT license. See the LICENSE file for more info.
